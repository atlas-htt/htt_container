# Custom worker image to build YARR on GitLab-CI at CERN

Based on: https://gitlab.cern.ch/gitlabci-examples/build_docker_image

## Some notes from there on using Kaniko

This method uses [Kaniko](https://github.com/GoogleContainerTools/kaniko) for building your images. For upstream documentation please check <https://docs.gitlab.com/ee/ci/docker/using_kaniko.html>.

This Docker-build method can run in our default runners.

We recommend using the CERN version of the Kaniko image: `gitlab-registry.cern.ch/ci-tools/docker-image-builder`. Check <https://gitlab.cern.ch/ci-tools/docker-image-builder> for further details about the image.
