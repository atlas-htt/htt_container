#!/bin/bash
#

rsync="/usr/bin/rsync"
if [ ! -x $rsync ]
then
    echo ERROR: $rsync not found
    exit 1
fi

# SSH will be used to connect to LXPLUS and there check if the EOS folder exists
ssh="/usr/bin/ssh"
if [ ! -x $ssh ]
then
    echo ERROR: $ssh not found
    exit 1
fi

# Check that the connection to lxplus is possible
$ssh -o StrictHostKeyChecking=no -o GSSAPIAuthentication=yes -o GSSAPITrustDNS=yes -o GSSAPIDelegateCredentials=yes $EOS_ACCOUNT_USERNAME@lxplus.cern.ch 'whoami; echo 0' > /dev/null 2>&1
if [ $? -ne 0 ]
then
    echo ERROR: Not possible to connect to lxplus.cern.ch with user \"$EOS_ACCOUNT_USERNAME\"
    exit 1
fi

# Rsync files with EOS
$rsync --recursive --delete --verbose -e "ssh -o StrictHostKeyChecking=no -o GSSAPIAuthentication=yes -o GSSAPITrustDNS=yes -o GSSAPIDelegateCredentials=yes" $CI_OUTPUT_DIR/ $EOS_ACCOUNT_USERNAME@lxplus.cern.ch:$EOS_PATH/
if [ $? -ne 0 ]
then
    echo ERROR: Rsync to \"$EOS_PATH\" via lxplus.cern.ch, failed
    exit 1
fi


